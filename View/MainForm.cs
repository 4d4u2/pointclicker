﻿using Emgu.CV;
using Emgu.CV.Structure;
using PointClicker.Controller;
using PointClicker.Utility;
using System;
using System.Windows.Forms;

namespace PointClicker.View
{
    /// <summary>
    /// Defines the main form for the application
    /// </summary>
    public partial class MainForm : Form
    {
        #region Private Variables

        private MainController _controller;
        private int _counter = 0;

        #endregion

        #region Main Constructor

        /// <summary>
        /// Defines the main contructor associated with the application
        /// </summary>
        public MainForm()
        {
            InitializeComponent();
            _controller = new MainController();
            _controller.OnUpdateImage += OnUpdateImage;
        }

        #endregion

        #region Form Events

        /// <summary>
        /// Defines the form loading logic
        /// </summary>
        /// <param name="sender">The sender control</param>
        /// <param name="arguments">The arguments associated with form events</param>
        private void OnLoad(object sender, EventArgs arguments)
        {
            UpdateStatus(_controller.GetNextWorldPoint());
        }

        #endregion

        #region Menu Events

        /// <summary>
        /// Click on the about box menu item
        /// </summary>
        /// <param name="sender">The sender control</param>
        /// <param name="arguments">The arguments associated with the event</param>
        private void OnAboutBoxClick(object sender, EventArgs arguments)
        {
            new AboutBox().ShowDialog(this);
        }

        /// <summary>
        /// Defines the functionality of loading an image into the system
        /// </summary>
        /// <param name="sender">The sender control that we are loading</param>
        /// <param name="arguments">The arguments that we are loading</param>
        private void OnImageLoad(object sender, EventArgs arguments)
        {
            var result = openFileDialog.ShowDialog(this);
            if (result == DialogResult.OK) _controller.LoadImage(openFileDialog.FileName);
        }

        /// <summary>
        /// Event handler for saving the result
        /// </summary>
        /// <param name="sender">The sender control</param>
        /// <param name="arguments">The arguments that we are saving</param>
        private void OnSave(object sender, EventArgs arguments)
        {
            saveFileDialog.FileName = "grid_1.csv";
            var result = saveFileDialog.ShowDialog(this);

            if (result == DialogResult.OK)
            {
                var grid1 = _controller.GetGrid1(); var grid2 = _controller.GetGrid2();
                grid1.AddRange(grid2);
                SaveUtility.Save(saveFileDialog.FileName, grid1);
            }

        }

        /// <summary>
        /// Click on the properties menu item
        /// </summary>
        /// <param name="sender">The sender control</param>
        /// <param name="arguments">The arguments associated with the event</param>
        private void OnPropertiesClick(object sender, EventArgs arguments)
        {
            new SettingsForm(_controller.GetSettings()).ShowDialog(this);
        }

        /// <summary>
        /// Handles the clicking of the exit button
        /// </summary>
        /// <param name="sender">The sender control</param>
        /// <param name="arguments">The associated event arguments</param>
        private void OnExitClick(object sender, EventArgs arguments)
        {
            Application.Exit();
        }

        #endregion

        #region Controller Events

        /// <summary>
        /// Raised when the image needs to be updated on the main display
        /// </summary>
        /// <param name="image">The image that we are updating</param>
        private void OnUpdateImage(Image<Bgr, byte> image)
        {
            pictureBox.Image = image.Bitmap;
            image.Save(string.Format("Images/image_{0:0000}.jpg", _counter++));
        }

        #endregion

        #region Picture Box Events

        /// <summary>
        /// Add the clicking of the mouse picture box
        /// </summary>
        /// <param name="sender">The sender control</param>
        /// <param name="arguments">The arguments associated with the event handler</param>
        private void OnPictureClick(object sender, MouseEventArgs arguments)
        {
            _controller.SendClick(pictureBox, arguments.Location);
            UpdateStatus(_controller.GetNextWorldPoint());
            if (_controller.IsGridComplete()) _controller.BuildGrid(this);
        }

        #endregion

        #region Utility Functions

        /// <summary>
        /// Update the status message 
        /// </summary>
        /// <param name="point">The point that is next in line</param>
        public void UpdateStatus(MCvPoint3D64f? point)
        {
            if (point == null) statusLabel.Text = "No more points expected!";
            else statusLabel.Text = string.Format("Next Point: <{0}, {1}, {2}>", point.Value.X, point.Value.Y, point.Value.Z);
        }

        #endregion
    }
}
