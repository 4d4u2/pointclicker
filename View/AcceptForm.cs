﻿using PointClicker.Controller;
using PointClicker.Model;
using System.Windows.Forms;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace PointClicker.View
{
    /// <summary>
    /// Defines the form for honing the associated grid
    /// </summary>
    public partial class AcceptForm : Form
    {
        #region Private Variables

        private AcceptController _controller;

        #endregion

        #region Main Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="settings">The associated settings</param>
        /// <param name="points">The clicked points that we are processing with</param>
        /// <param name="imageSize">The size of the image that we are working with</param>
        public AcceptForm(GridSettings settings, ClickedPoints points, Size imageSize)
        {
            InitializeComponent();
            _controller = new AcceptController(settings, points, imageSize);
        }

        #endregion

        #region Form Event Handlers

        /// <summary>
        /// Raised when the form loads
        /// </summary>
        /// <param name="sender">The sender control</param>
        /// <param name="arguments">The arguments associated with the event</param>
        private void OnFormLoad(object sender, EventArgs arguments)
        {
            squareSlider.Value = _controller.GetSquareSize();
        }

        #endregion

        #region Retrieve a Handle to the controller

        /// <summary>
        /// Retrieve a handle to the controller
        /// </summary>
        /// <returns>A handle to the associated controller</returns>
        public AcceptController GetController()
        {
            return _controller;
        }

        #endregion

        #region Retrieve the current points from the system

        /// <summary>
        /// Retrieve the grid points that we are working with
        /// </summary>
        /// <returns>The list of grid points associated with the form</returns>
        public List<GridPoint> GetPoints()
        {
            return _controller.GetPoints();
        }

        #endregion

        #region Slider Event Handlers

        /// <summary>
        /// Indicates that the distortion bar has scrolled
        /// </summary>
        /// <param name="sender">The sender control</param>
        /// <param name="arguments">The associated set of arguments</param>
        private void OnDistortionScroll(object sender, EventArgs arguments)
        {
            _controller.DistortPoints(distortionSlider.Value / 10.0);
        }

        /// <summary>
        /// Indicate that the square size has changed
        /// </summary>
        /// <param name="sender">The sender control</param>
        /// <param name="arguments">The event arguments</param>
        private void OnSquareScroll(object sender, EventArgs arguments)
        {
            _controller.ChangeSquare(squareSlider.Value);
        }

        #endregion

        #region Button Event Handlers

        /// <summary>
        /// Handles the clicking of the okay button
        /// </summary>
        /// <param name="sender">The sender control/param>
        /// <param name="arguments">The associated event arguments</param>
        private void OnOkayButtonClick(object sender, EventArgs arguments)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        /// <summary>
        /// Handles the clicking of the cancel button
        /// </summary>
        /// <param name="sender">The sender control</param>
        /// <param name="arguments">The associated event arguments</param>
        private void OnCancelButtonClick(object sender, EventArgs arguments)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        #endregion
    }
}