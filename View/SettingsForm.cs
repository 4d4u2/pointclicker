﻿using PointClicker.Controller;
using PointClicker.Model;
using System.Windows.Forms;

namespace PointClicker.View
{
    /// <summary>
    /// Code behind for the settings form
    /// </summary>
    public partial class SettingsForm : Form
    {
        #region Private Variables

        private SettingsController _controller;

        #endregion

        #region Main Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="settings">The associated settings aligned to the application</param>
        public SettingsForm(GridSettings settings)
        {
            InitializeComponent();
            _controller = new SettingsController(settings);
        }

        #endregion

        #region Form Events

        /// <summary>
        /// Defines the load event associated with the given form
        /// </summary>
        /// <param name="sender">The sender control associated with the form</param>
        /// <param name="arguments">The arguments associated with the form</param>
        private void OnLoad(object sender, System.EventArgs arguments)
        {
            _controller.UpdateControls(this); gridXBox.Select();
        }

        #endregion

        #region Control Properties

        /// <summary>
        /// Retrieve the textbox for storing the grid count in the x-direction
        /// </summary>
        public TextBox GridXBox { get { return gridXBox; } }

        /// <summary>
        /// Rerieve the textbox for storing the grid count in the y-direction
        /// </summary>
        public TextBox GridYBox { get { return gridYBox; } }

        /// <summary>
        /// Retrieve the textbox for storing to physical block size
        /// </summary>
        public TextBox BlockBox { get { return blockBox; } }

        /// <summary>
        /// Get the text box for storing the size of the search box
        /// </summary>
        public TextBox SquareBox { get { return squareBox; } }

        /// <summary>
        /// The error provider associated with this application
        /// </summary>
        public ErrorProvider Provider { get { return errorProvider; } }

        #endregion

        #region Control Events

        /// <summary>
        /// Event handler for clicking the accept button
        /// </summary>
        /// <param name="sender">The sender control</param>
        /// <param name="arguments">The arguments associted with the event handler</param>
        private void OnAcceptButtonClick(object sender, System.EventArgs arguments)
        {
            var success = _controller.Submit(this);
            if (success) Close();
        }

        /// <summary>
        /// Event handler for clicking the cancel button
        /// </summary>
        /// <param name="sender">The sender control</param>
        /// <param name="arguments">The arguments associated with the event handler</param>
        private void OnCancelButtonClick(object sender, System.EventArgs arguments)
        {
            Close();
        }

        #endregion

        #region Validation Event Handlers

        /// <summary>
        /// Validate the gridX field
        /// </summary>
        /// <param name="sender">The sender control</param>
        /// <param name="arguments">Event Arguments</param>
        private void OnValidatingGridX(object sender, System.ComponentModel.CancelEventArgs arguments)
        {
            var error = _controller.ValidateGridSize(gridXBox.Text);
            errorProvider.SetError(gridXBox, error);
        }

        /// <summary>
        /// Validate the gridY field
        /// </summary>
        /// <param name="sender">The sender control</param>
        /// <param name="arguments">Event Arguments</param>
        private void OnValidatingGridY(object sender, System.ComponentModel.CancelEventArgs arguments)
        {
            var error = _controller.ValidateGridSize(gridYBox.Text);
            errorProvider.SetError(gridYBox, error);
        }

        /// <summary>
        /// Validate the block size field
        /// </summary>
        /// <param name="sender">The sender control</param>
        /// <param name="arguments">Event arguments</param>
        private void OnValidatingBlockSize(object sender, System.ComponentModel.CancelEventArgs arguments)
        {
            var error = _controller.ValidateBlockSize(blockBox.Text);
            errorProvider.SetError(blockBox, error);
        }

        /// <summary>
        /// Validate the square size field
        /// </summary>
        /// <param name="sender">The sender control</param>
        /// <param name="arguments">Event arguments</param>
        private void OnValidatingSquareSize(object sender, System.ComponentModel.CancelEventArgs arguments)
        {
            var error = _controller.ValidateSquareSize(squareBox.Text);
            errorProvider.SetError(squareBox, error);
        }

        #endregion
    }
}
