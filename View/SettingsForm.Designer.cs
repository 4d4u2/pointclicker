﻿namespace PointClicker.View
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.gridXLabel = new System.Windows.Forms.Label();
            this.gridYLabel = new System.Windows.Forms.Label();
            this.blockSizeLabel = new System.Windows.Forms.Label();
            this.SearchBoxSizeLabel = new System.Windows.Forms.Label();
            this.gridXBox = new System.Windows.Forms.TextBox();
            this.gridYBox = new System.Windows.Forms.TextBox();
            this.blockBox = new System.Windows.Forms.TextBox();
            this.squareBox = new System.Windows.Forms.TextBox();
            this.cancelButton = new System.Windows.Forms.Button();
            this.acceptButton = new System.Windows.Forms.Button();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.gridDimBox = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.gridDimBox.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // gridXLabel
            // 
            this.gridXLabel.AutoSize = true;
            this.gridXLabel.Location = new System.Drawing.Point(14, 34);
            this.gridXLabel.Name = "gridXLabel";
            this.gridXLabel.Size = new System.Drawing.Size(48, 17);
            this.gridXLabel.TabIndex = 11;
            this.gridXLabel.Text = "Grid X";
            // 
            // gridYLabel
            // 
            this.gridYLabel.AutoSize = true;
            this.gridYLabel.Location = new System.Drawing.Point(14, 63);
            this.gridYLabel.Name = "gridYLabel";
            this.gridYLabel.Size = new System.Drawing.Size(48, 17);
            this.gridYLabel.TabIndex = 12;
            this.gridYLabel.Text = "Grid Y";
            // 
            // blockSizeLabel
            // 
            this.blockSizeLabel.AutoSize = true;
            this.blockSizeLabel.Location = new System.Drawing.Point(13, 34);
            this.blockSizeLabel.Name = "blockSizeLabel";
            this.blockSizeLabel.Size = new System.Drawing.Size(35, 17);
            this.blockSizeLabel.TabIndex = 13;
            this.blockSizeLabel.Text = "Size";
            // 
            // SearchBoxSizeLabel
            // 
            this.SearchBoxSizeLabel.AutoSize = true;
            this.SearchBoxSizeLabel.Location = new System.Drawing.Point(13, 26);
            this.SearchBoxSizeLabel.Name = "SearchBoxSizeLabel";
            this.SearchBoxSizeLabel.Size = new System.Drawing.Size(35, 17);
            this.SearchBoxSizeLabel.TabIndex = 14;
            this.SearchBoxSizeLabel.Text = "Size";
            // 
            // gridXBox
            // 
            this.gridXBox.Location = new System.Drawing.Point(68, 29);
            this.gridXBox.Name = "gridXBox";
            this.gridXBox.Size = new System.Drawing.Size(162, 22);
            this.gridXBox.TabIndex = 1;
            this.gridXBox.Validating += new System.ComponentModel.CancelEventHandler(this.OnValidatingGridX);
            // 
            // gridYBox
            // 
            this.gridYBox.Location = new System.Drawing.Point(68, 63);
            this.gridYBox.Name = "gridYBox";
            this.gridYBox.Size = new System.Drawing.Size(162, 22);
            this.gridYBox.TabIndex = 2;
            this.gridYBox.Validating += new System.ComponentModel.CancelEventHandler(this.OnValidatingGridY);
            // 
            // blockBox
            // 
            this.blockBox.Location = new System.Drawing.Point(67, 31);
            this.blockBox.Name = "blockBox";
            this.blockBox.Size = new System.Drawing.Size(162, 22);
            this.blockBox.TabIndex = 3;
            this.blockBox.Validating += new System.ComponentModel.CancelEventHandler(this.OnValidatingBlockSize);
            // 
            // squareBox
            // 
            this.squareBox.Location = new System.Drawing.Point(67, 26);
            this.squareBox.Name = "squareBox";
            this.squareBox.Size = new System.Drawing.Size(162, 22);
            this.squareBox.TabIndex = 4;
            this.squareBox.Validating += new System.ComponentModel.CancelEventHandler(this.OnValidatingSquareSize);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(198, 296);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 39);
            this.cancelButton.TabIndex = 5;
            this.cancelButton.Text = "&Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.OnCancelButtonClick);
            // 
            // acceptButton
            // 
            this.acceptButton.Location = new System.Drawing.Point(117, 296);
            this.acceptButton.Name = "acceptButton";
            this.acceptButton.Size = new System.Drawing.Size(75, 39);
            this.acceptButton.TabIndex = 6;
            this.acceptButton.Text = "&Accept";
            this.acceptButton.UseVisualStyleBackColor = true;
            this.acceptButton.Click += new System.EventHandler(this.OnAcceptButtonClick);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // gridDimBox
            // 
            this.gridDimBox.Controls.Add(this.gridXLabel);
            this.gridDimBox.Controls.Add(this.gridYLabel);
            this.gridDimBox.Controls.Add(this.gridXBox);
            this.gridDimBox.Controls.Add(this.gridYBox);
            this.gridDimBox.Location = new System.Drawing.Point(12, 22);
            this.gridDimBox.Name = "gridDimBox";
            this.gridDimBox.Size = new System.Drawing.Size(261, 109);
            this.gridDimBox.TabIndex = 15;
            this.gridDimBox.TabStop = false;
            this.gridDimBox.Text = "Grid Dimensions";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.blockSizeLabel);
            this.groupBox1.Controls.Add(this.blockBox);
            this.groupBox1.Location = new System.Drawing.Point(13, 137);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(260, 78);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Actual Block Size (mm)";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.squareBox);
            this.groupBox2.Controls.Add(this.SearchBoxSizeLabel);
            this.groupBox2.Location = new System.Drawing.Point(13, 222);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(260, 68);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Point Search Box Size";
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(285, 347);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gridDimBox);
            this.Controls.Add(this.acceptButton);
            this.Controls.Add(this.cancelButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SettingsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.OnLoad);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.gridDimBox.ResumeLayout(false);
            this.gridDimBox.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label gridXLabel;
        private System.Windows.Forms.Label gridYLabel;
        private System.Windows.Forms.Label blockSizeLabel;
        private System.Windows.Forms.Label SearchBoxSizeLabel;
        private System.Windows.Forms.TextBox gridXBox;
        private System.Windows.Forms.TextBox gridYBox;
        private System.Windows.Forms.TextBox blockBox;
        private System.Windows.Forms.TextBox squareBox;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button acceptButton;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox gridDimBox;
    }
}