﻿using PointClicker.Model;
using System.Collections.Generic;
using System.IO;

namespace PointClicker.Utility
{
    /// <summary>
    /// Defines the utility for saving the result
    /// </summary>
    public class SaveUtility
    {
        #region Save Functionality

        /// <summary>
        /// Save the results to disk
        /// </summary>
        /// <param name="path">The path that we are saving to</param>
        /// <param name="points">The points that we are saving</param>
        public static void Save(string path, List<GridPoint> points)
        {
            using (var writer = new StreamWriter(path))
            {
                foreach (var point in points)
                {
                    writer.WriteLine("{0} {1}", point.ImagePoint.X, point.ImagePoint.Y);
                }
            }
        }

        #endregion
    }
}
