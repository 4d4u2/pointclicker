﻿using Emgu.CV;
using Emgu.CV.Structure;
using PointClicker.Model;
using System.Collections.Generic;
using System.Drawing;
using System;

namespace PointClicker.Utility
{
    /// <summary>
    /// Defines the logic to render on the associated image
    /// </summary>
    public class ImageRenderer
    {
        #region Render Points

        /// <summary>
        /// Render the points that we are dealing with
        /// </summary>
        /// <param name="image">The image that we are rendering on</param>
        /// <param name="points">The points that we are dealing with</param>
        public static void RenderPoints(Image<Bgr, byte> image, List<GridPoint> points)
        {
            foreach (var point in points)
            {
                CvInvoke.Circle(image, MakePoint(point), 10, new MCvScalar(0, 0, 255), 10);
            }
        }

        #endregion

        #region Render Grid

        /// <summary>
        /// Defines the logic to render the associated grid
        /// </summary>
        /// <param name="image">The image that we are rendering upon</param>
        /// <param name="grid">The grid that we are rendering</param>
        /// <param name="gridIndex">The index of the grid</param>
        public static void RenderGrid(Image<Bgr, byte> image, List<GridPoint> grid, int gridIndex)
        {
            var color = gridIndex == 0 ? new MCvScalar(0, 255, 0) : new MCvScalar(0, 0, 255);

            foreach (var point in grid)
            {
                CvInvoke.Circle(image, MakePoint(point), 10, color, 10);
            }
        }

        #endregion

        #region Rendering of Squares Logic goes here

        /// <summary>
        /// Defines the logic to render the associated squqares
        /// </summary>
        /// <param name="image">The image that we are rendering upon</param>
        /// <param name="squares">The squares that we are rendering</param>
        /// <param name="squareSize">The size of the square</param>
        public static void RenderSquares(Image<Bgr, byte> image, List<GridPoint> squares, int squareSize)
        {
            foreach (var square in squares)
            {
                var point = MakePoint(square);
                var rectangle = new Rectangle(point.X - squareSize, point.Y - squareSize, squareSize * 2, squareSize * 2);
                CvInvoke.Rectangle(image, rectangle, new MCvScalar(0, 0, 255), 2);
            }
        }

        #endregion

        #region Utility methods

        /// <summary>
        /// Converts a grid point into a normal point
        /// </summary>
        /// <param name="point">The point that we are rendering</param>
        /// <returns>The resultant point</returns>
        private static Point MakePoint(GridPoint point)
        {
            var u = (int)Math.Round(point.ImagePoint.X);
            var v = (int)Math.Round(point.ImagePoint.Y);
            return new Point(u, v);
        }

        #endregion
    }
}
