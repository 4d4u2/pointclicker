﻿using Emgu.CV.Structure;
using LMDotNet;
using PointClicker.Model;
using System;

namespace PointClicker.Utility
{
    /// <summary>
    /// Defines the logic to perform Zhang Distortion
    /// </summary>
    public class Distorter
    {
        #region Private Variables

        Distortion _distortionParams;
        private MCvPoint2D64f _expectedCoordinate;

        #endregion

        #region Add Distortion

        /// <summary>
        /// Defines the logic to distort a given point
        /// </summary>
        /// <param name="parameters">The distoriton parameters that we are working with</param>
        /// <param name="point">The point that we are distorting</param>
        /// <returns>The distorted location of the point</returns>
        public MCvPoint2D64f Distort(Distortion parameters, MCvPoint2D64f point)
        {
            var screen = Image2Screen(parameters, point);
            var r2 = screen.X * screen.X + screen.Y * screen.Y; 

            var k1 = parameters.K1;  
            var xu = screen.X; var yu = screen.Y;

            var xd = xu * (1 + k1 * r2);
            var yd = yu * (1 + k1 * r2);

            return Screen2Image(parameters, new MCvPoint2D64f(xd,yd));
        }

        #endregion

        #region Remove Distortion

        /// <summary>
        /// Defines the logic to undistort a given point
        /// </summary>
        /// <param name="point">The point that we are undistorting</param>
        /// <returns>The undistorted location of the given point</returns>
        public MCvPoint2D64f UnDistort(Distortion parameters, MCvPoint2D64f point)
        {
            _expectedCoordinate = point; _distortionParams = parameters;
            var solver = new LMSolver(); 
            var result = solver.Solve(CostFunction, new double[] { _expectedCoordinate.X, _expectedCoordinate.Y });
            return new MCvPoint2D64f(result.OptimizedParameters[0], result.OptimizedParameters[1]);
        }

        /// <summary>
        /// The function that we are minimizing
        /// </summary>
        /// <param name="parameters">The parameters of the function</param>
        /// <param name="residuals">The associated residuals</param>
        public void CostFunction(double[] parameters, double[] residuals)
        {
            var point = new MCvPoint2D64f(parameters[0], parameters[1]);
            var solution = Distort(_distortionParams, point);
            residuals[0] = Math.Sqrt ( (_expectedCoordinate.X - solution.X) * (_expectedCoordinate.X - solution.X));
            residuals[1] = Math.Sqrt ( (_expectedCoordinate.Y - solution.Y) * (_expectedCoordinate.Y - solution.Y) );
        }

        #endregion

        #region Conversion Utilities

        /// <summary>
        /// Convert a screen coordinate to an image coordinate
        /// </summary>
        /// <param name="parameters">The associated set of distortion parameters</param>
        /// <param name="point">The point that we are converting</param>
        /// <returns>The resultant coordinate</returns>
        private static MCvPoint2D64f Screen2Image(Distortion parameters, MCvPoint2D64f point)
        {
            var x = point.X * parameters.Focal + parameters.CX;
            var y = point.Y * parameters.Focal + parameters.CY;
            return new MCvPoint2D64f(x, y);
        }

        /// <summary>
        /// Convert an image coordinate to a screen coordinate
        /// </summary>
        /// <param name="parameters">The distortion parameters that we are dealing with</param>
        /// <param name="point">The point that we are converting</param>
        /// <returns>The resultant coordinate</returns>
        private static MCvPoint2D64f Image2Screen(Distortion parameters, MCvPoint2D64f point)
        {
            var x = (point.X - parameters.CX) / parameters.Focal;
            var y = (point.Y - parameters.CY) / parameters.Focal;
            return new MCvPoint2D64f(x, y);
        }

        #endregion
    }
}