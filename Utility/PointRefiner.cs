﻿using Emgu.CV;
using Emgu.CV.Features2D;
using Emgu.CV.Structure;
using System;
using System.Drawing;

namespace PointClicker.Utility
{
    /// <summary>
    /// Refine the point 
    /// </summary>
    public class PointRefiner
    {
        #region Entry Point Logic

        /// <summary>
        /// Defines the logic to refine the selected point
        /// </summary>
        /// <param name="image">The image that we are calculating against</param>
        /// <param name="point">The point that we are dealing with</param>
        /// <param name="windowSize">The size of the window that we are working with</param>
        /// <returns></returns>
        public static MCvPoint2D64f Refine(Image<Bgr, byte> image, MCvPoint2D64f point, int windowSize) 
        {
            // Get the ROI
            var roi = GetROI(image.Bitmap, point, windowSize);

            // Make Image Gray
            var gray = new Image<Gray, byte>(roi.Width, roi.Height);
            CvInvoke.CvtColor(roi, gray, Emgu.CV.CvEnum.ColorConversion.Bgr2Gray);
            CvInvoke.MedianBlur(gray, gray, 3);

            // Detect the corner
            GFTTDetector detector = new GFTTDetector(1, 1e-25, 1, 5);
            var keyPoints = detector.Detect(gray);
            if (keyPoints.GetLength(0) == 0) return point;
 
            // Refine the corner
            var bigGray = new Image<Gray, byte>(image.Size); CvInvoke.CvtColor(image, bigGray, Emgu.CV.CvEnum.ColorConversion.Bgr2Gray);
            var size = 11;

            // Calculate the point
            var points = new Matrix<float>(1, 2, 1);
            var rectangle = CalculateROI(point, windowSize);
            points[0, 0] = keyPoints[0].Point.X + rectangle.X; points[0, 1] = keyPoints[0].Point.Y + rectangle.Y;
 
            // Refine
            CvInvoke.CornerSubPix(bigGray, points, new Size(size, size), new Size(-1, -1), new MCvTermCriteria(40, 1e-3));

            // Return the result
            return new MCvPoint2D64f(points[0, 0], points[0, 1]);
        }

        #endregion

        #region Utility Methods

        /// <summary>
        /// Calculate the ROI that we are using in this application
        /// </summary>
        /// <param name="point">The location of the ROI</param>
        /// <param name="windowSize">The size of the window</param>
        /// <returns>The resultant rectangle</returns>
        private static Rectangle CalculateROI(MCvPoint2D64f point, int windowSize)
        {
            var startX = (int)Math.Floor(point.X - windowSize); var startY = (int)Math.Floor(point.Y - windowSize);
            return new Rectangle(startX, startY, windowSize * 2, windowSize * 2);    
        }

        /// <summary>
        /// Extract the region of interest
        /// </summary>
        /// <param name="image">The image that we are getting the ROI from</param>
        /// <param name="point">The point associated with the region of interest</param>
        /// <param name="windowSize">The size of the associated window</param>
        /// <returns>The resultant region of interst</returns>
        private static Image<Bgr, byte> GetROI(Bitmap image, MCvPoint2D64f point, int windowSize)
        {
            var roiRectangle = CalculateROI(point, windowSize);
            var result = new Image<Bgr, byte>(roiRectangle.Size);
            using (var graphics = Graphics.FromImage(result.Bitmap)) graphics.DrawImage(image, 0, 0, roiRectangle, GraphicsUnit.Pixel);
            return result;
        }

        #endregion
    }
}