﻿namespace PointClicker.Model
{
    /// <summary>
    /// Defines the grid settings in the context of the application
    /// </summary>
    public class GridSettings
    {
        #region Main Constructor

        /// <summary>
        /// Main Constructor - Set the grid settings to default values
        /// </summary>
        public GridSettings()
        {
            GridX = 5; GridY = 8;
            BlockSize = 30;
            SquareSize = 21;
            Distortion = 0;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// The amount of blocks in the x-direction within the grid
        /// </summary>
        public int GridX { get; set; }

        /// <summary>
        /// The amount of blocks in the y-directiion within the grid
        /// </summary>
        public int GridY { get; set; }

        /// <summary>
        /// The actual dimension of a block in the grid
        /// </summary>
        public double BlockSize { get; set; }

        /// <summary>
        /// The size of the search square in pixels that we are using
        /// </summary>
        public int SquareSize { get; set; }

        /// <summary>
        /// The distortion - defines as a radial distortion coefficient
        /// </summary>
        public double Distortion { get; set; }

        #endregion
    }
}