﻿namespace PointClicker.Model
{
    /// <summary>
    /// Defines a model for holding distortion values
    /// </summary>
    public class Distortion
    {
        #region Constructor

        /// <summary>
        /// Main constructor
        /// </summary>
        /// <param name="k1">The first radial distortion term</param>
        /// <param name="focal">The focal length term</param>
        /// <param name="cx">The optical center x-coordinate</param>
        /// <param name="cy">The optical center y-coordinate</param>
        public Distortion(double k1, double focal, double cx, double cy)
        {
            K1 = k1; Focal = focal; CX = cx; CY = cy;
        }
        
        #endregion

        #region Public Properties

        /// <summary>
        /// Defines the radial distortion term
        /// </summary>
        public double K1 { get; private set; }

        /// <summary>
        /// The associated focal length term
        /// </summary>
        public double Focal { get; private set; }

        /// <summary>
        /// The optical center x-coordinate
        /// </summary>
        public double CX { get; private set; }

        /// <summary>
        /// The optical center y-coordinate
        /// </summary>
        public double CY { get; private set; }

        #endregion
    }
}