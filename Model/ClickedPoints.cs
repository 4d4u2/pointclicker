﻿using Emgu.CV.Structure;
using System.Collections.Generic;
using System;

namespace PointClicker.Model
{
    /// <summary>
    /// Defines the logic for clicking the associated set of points
    /// </summary>
    public class ClickedPoints
    {
        #region Private Variables

        private GridSettings _settings;
        private List<MCvPoint3D64f> _worldPoints;
        private List<GridPoint> _points;

        #endregion

        #region Main Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="gridSettings">The settings associated with the application</param>
        public ClickedPoints(GridSettings gridSettings)
        {
            _settings = gridSettings;
            _worldPoints = BuildWorldPoints();
            _points = new List<GridPoint>();
        }

        #endregion

        #region Functionality to add point

        /// <summary>
        /// Add a point to the collection
        /// </summary>
        /// <param name="point">The point that is being added</param>
        public void AddPoint(MCvPoint2D64f point)
        {
            var removed = TryPointRemoval(point);
            if (removed) return;

            if (IsComplete()) return;
            var worldPoint = GetNextWorldPoint();
            _points.Add(new GridPoint(worldPoint.Value, point));
        }

        /// <summary>
        /// Attempt a point removal
        /// </summary>
        /// <param name="point">The point that we are trying to remove</param>
        /// <returns>True if a point was removed, else false</returns>
        private bool TryPointRemoval(MCvPoint2D64f point)
        {
            for (var i = 0; i< _points.Count; i++)
            {
                var xDiff = _points[i].ImagePoint.X - point.X;
                var yDiff = _points[i].ImagePoint.Y - point.Y;
                var distance = Math.Sqrt(xDiff * xDiff + yDiff * yDiff);

                if (distance < _settings.SquareSize)
                {
                    int remainingCount = _points.Count;
                    for (int j = i; j < remainingCount; j++) _points.RemoveAt(i);
                    return true;
                }
            }

            return false;
        }

        #endregion

        #region State Retrieval

        /// <summary>
        /// Retrieve the collection of grid points
        /// </summary>
        /// <returns>The list of associated grid points</returns>
        public List<GridPoint> GetPoints()
        {
            // Update the scene points because this might have changed
            _worldPoints = BuildWorldPoints();
            for (int i = 0; i < _points.Count; i++)
            {
                _points[i].ScenePoint = new MCvPoint3D64f(_worldPoints[i].X, _worldPoints[i].Y, _worldPoints[i].Z);
            }

            // Retrieve the actual points
            return _points;
        }

        /// <summary>
        /// Check to see if the point collection is enough to make a grid
        /// </summary>
        /// <returns>True if the collection is complete</returns>
        public bool IsComplete()
        {
            return _points.Count == 4;
        }

        /// <summary>
        /// Retrieve the next world point in the collection
        /// </summary>
        /// <returns>The next world point in the collection</returns>
        public MCvPoint3D64f? GetNextWorldPoint()
        {
            if (IsComplete()) return null;
            return _worldPoints[_points.Count];
        }

        #endregion

        #region Clear Functionality

        /// <summary>
        /// Defines the logic to clear the associated point collection
        /// </summary>
        public void Clear()
        {
            _points.Clear();
        }

        #endregion

        #region Defines a Utility Function to Build world points

        /// <summary>
        /// Build the associated list of world points
        /// </summary>
        /// <returns>The list of points that we have added</returns>
        private List<MCvPoint3D64f> BuildWorldPoints()
        {
            var result = new List<MCvPoint3D64f>
            {
                new MCvPoint3D64f(0, 0, 0),
                new MCvPoint3D64f(_settings.BlockSize * _settings.GridX, 0, 0),
                new MCvPoint3D64f(_settings.BlockSize * _settings.GridX, _settings.BlockSize * _settings.GridY, 0),
                new MCvPoint3D64f(0, _settings.BlockSize * _settings.GridY, 0)
            };

            return result;
        }

        #endregion
    }
}