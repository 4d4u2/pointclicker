﻿using Emgu.CV.Structure;

namespace PointClicker.Model
{
    /// <summary>
    /// Defines a grid point within the image
    /// </summary>
    public class GridPoint
    {
        #region Constructors

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="scenePoint">The scene point that is being added</param>
        /// <param name="imagePoint">The image point that is being added</param>
        public GridPoint(MCvPoint3D64f scenePoint, MCvPoint2D64f imagePoint)
        {
            ScenePoint = scenePoint; ImagePoint = imagePoint; OriginalImagePoint = imagePoint;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Defines the scene point that we are working with
        /// </summary>
        public MCvPoint3D64f ScenePoint { get; set; }
 
        /// <summary>
        /// The image point that we are working with
        /// </summary>
        public MCvPoint2D64f ImagePoint { get; set; }

        /// <summary>
        /// The original image point that we are working with
        /// </summary>
        public MCvPoint2D64f OriginalImagePoint { get; set; }

        #endregion
    }
}