﻿using Emgu.CV;
using Emgu.CV.Structure;
using PointClicker.Model;
using PointClicker.Utility;
using PointClicker.View;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace PointClicker.Controller
{
    /// <summary>
    /// The main controller associated with the application
    /// </summary>
    public class MainController
    {
        #region Private Variables

        private Image<Bgr, byte> _image;
        private GridSettings _settings;
        private ClickedPoints _clickedPoints;
        private List<GridPoint> _grid1;
        private List<GridPoint> _grid2;
        private AcceptForm _acceptForm;

        #endregion

        #region Public Event Definition

        public event Action<Image<Bgr, byte>> OnUpdateImage; 

        #endregion

        #region Main Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="form">The form that this controller is controlling</param>
        public MainController()
        {
            _settings = new GridSettings();
            _clickedPoints = new ClickedPoints(_settings);
            _grid1 = new List<GridPoint>(); _grid2 = new List<GridPoint>();
        }

        #endregion

        #region Image Load

        /// <summary>
        /// Defines the functionality to load an image into the system
        /// </summary>
        /// <param name="path">The path that we are loading</param>
        public void LoadImage(string path)
        {
            _image = new Image<Bgr, byte>(path);
            if (OnUpdateImage != null) OnUpdateImage(_image);
        }

        #endregion

        #region Getters

        /// <summary>
        /// Retrieve the associated grid settings
        /// </summary>
        /// <returns>The associated set of grid settings</returns>
        public GridSettings GetSettings()
        {
            return _settings;
        }

        /// <summary>
        /// Retrieve the first grid
        /// </summary>
        /// <returns>The list of the first grid</returns>
        public List<GridPoint> GetGrid1()
        {
            return _grid1;
        }

        /// <summary>
        /// Retrieve the second grid
        /// </summary>
        /// <returns>The list of second grid</returns>
        public List<GridPoint> GetGrid2()
        {
            return _grid2;
        }

        #endregion

        #region Handles the click on the image

        /// <summary>
        /// Recieve a click from the picture box
        /// </summary>
        /// <param name="pictureBox">The picture box that we are getting the value for</param>
        /// <param name="location">The location of the picture box</param>
        public void SendClick(PictureBox pictureBox, Point location)
        {
            if (_image == null || IsFinished()) return;

            int X,Y; CoordinateConverter.Convert(pictureBox, out X, out Y, location.X, location.Y);

            var point = new MCvPoint2D64f((double)X, (double)Y);
            var refinedPoint = PointRefiner.Refine(_image, point, _settings.SquareSize);

            _clickedPoints.AddPoint(refinedPoint); RenderClick();
        }

        /// <summary>
        /// Retrieve the next world point to retrieve
        /// </summary>
        /// <returns>The next world point that we want to retrieve</returns>
        public MCvPoint3D64f? GetNextWorldPoint()
        {
            return _clickedPoints.GetNextWorldPoint();
        }

        /// <summary>
        /// Test to see if the associated grid is complete
        /// </summary>
        /// <returns>Perform the associated grid complete check</returns>
        public bool IsGridComplete()
        {
            return _clickedPoints.IsComplete();
        }

        #endregion

        #region Build Grid Functionality

        /// <summary>
        /// Defines the functionality to build a grid
        /// </summary>
        /// <param name="form">The form that we are displaying</param>
        public void BuildGrid(MainForm form)
        {
            _acceptForm = new AcceptForm(_settings, _clickedPoints, _image.Size);
            OnGridParametersChange();
            _acceptForm.GetController().OnChange += OnGridParametersChange;
            var result = _acceptForm.ShowDialog(form);
            _acceptForm.GetController().OnChange -= OnGridParametersChange;

            if (result == DialogResult.OK) AddGrid();
            _clickedPoints.Clear(); RenderClick();
        }

        /// <summary>
        /// Defines the logic to add a new grid to the system
        /// </summary>
        private void AddGrid()
        {
            var squares = _acceptForm.GetPoints();
            var grid = ActiveGrid(); grid.Clear();

            foreach (var square in squares)
            {
                var point = PointRefiner.Refine(_image, square.ImagePoint, _settings.SquareSize);
                square.ImagePoint = point;
                grid.Add(square);
            }
        }

        /// <summary>
        /// Event handler to indicate that the grid parameters have changed
        /// </summary>
        private void OnGridParametersChange()
        {
            var squares = _acceptForm.GetPoints(); RenderSquares(squares);
        }

        #endregion

        #region Grid Point Render Logic

        /// <summary>
        /// Render the set of clicks that we have received
        /// </summary>
        private void RenderClick()
        {
            var displayImage = _image.Clone();
            if (_grid1.Count > 0) ImageRenderer.RenderGrid(displayImage, _grid1, 0);
            if (_grid2.Count > 0) ImageRenderer.RenderGrid(displayImage, _grid2, 1);
            ImageRenderer.RenderPoints(displayImage, _clickedPoints.GetPoints());
            if  (OnUpdateImage != null) OnUpdateImage(displayImage);
        }

        /// <summary>
        /// Defines the logic to render squares on the screen
        /// </summary>
        /// <param name="squares">The squares that are being rendered</param>
        private void RenderSquares(List<GridPoint> squares)
        {
            var displayImage = _image.Clone();
            if (_grid1.Count > 0) ImageRenderer.RenderGrid(displayImage, _grid1, 0);
            if (_grid2.Count > 0) ImageRenderer.RenderGrid(displayImage, _grid2, 1);
            ImageRenderer.RenderSquares(displayImage, squares, _settings.SquareSize);
            if (OnUpdateImage != null) OnUpdateImage(displayImage);
        }

        #endregion

        #region Utility Methods

        /// <summary>
        /// Retrieves a pointer to the current active grid
        /// </summary>
        /// <returns>The current active grid</returns>
        private List<GridPoint> ActiveGrid()
        {
            return _grid1.Count > 0 ? _grid2 : _grid1;
        }

        /// <summary>
        /// Test to see if we are finished
        /// </summary>
        /// <returns>True if we are finished</returns>
        private bool IsFinished()
        {
            return _grid1.Count > 0 && _grid2.Count > 0;
        }

        #endregion
    }
}