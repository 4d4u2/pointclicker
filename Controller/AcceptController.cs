﻿using Emgu.CV.Structure;
using PointClicker.Model;
using PointClicker.Utility;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace PointClicker.Controller
{
    /// <summary>
    /// Defines the controller for the accept form
    /// </summary>
    public class AcceptController
    {
        #region Private Variables

        private GridSettings _settings;
        private ClickedPoints _clickedPoints;
        private List<GridPoint> _points;
        private Size _imageSize;

        #endregion

        #region Public Events definitions

        /// <summary>
        /// Triggered when a change has occurred
        /// </summary>
        public event Action OnChange;

        #endregion

        #region Contructors

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="settings">The grid settings associated with accept controller</param>
        /// <param name="points">The points that we are clicking on</param>
        /// <param name="imageSize">The size of the image that we are processing</param>
        public AcceptController(GridSettings settings, ClickedPoints points, Size imageSize)
        {
            _settings = settings; _clickedPoints = points; _imageSize = imageSize;
            _points = GeneratePoints(); 
            
        }

        #endregion

        #region Getter Method for the points

        /// <summary>
        /// Retrieve the list of grid points
        /// </summary>
        /// <returns>The points that we are getting</returns>
        public List<GridPoint> GetPoints()
        {
            return _points;
        }

        #endregion

        #region State Getter Methods

        /// <summary>
        /// Retrieve the size of the associated squre
        /// </summary>
        /// <returns>The size of the squaren</returns>
        public int GetSquareSize()
        {
            return _settings.SquareSize;
        }

        #endregion

        #region Defines the Logic to Generate the Points

        /// <summary>
        /// Generate the points that we are working to be working with
        /// </summary>
        /// <returns>The point list that we have generated</returns>
        private List<GridPoint> GeneratePoints()
        {
            var H = HomographyFinder.Find(_clickedPoints);

            var points = new List<GridPoint>();

            for (int row = 0; row <= _settings.GridY; row++)
            {
                for (int column = 0; column <= _settings.GridX; column++)
                {
                    var sceneX = column * _settings.BlockSize; var sceneY = row * _settings.BlockSize;

                    var X = sceneX * H.Data[0, 0] + sceneY * H.Data[0, 1] + H.Data[0, 2];
                    var Y = sceneX * H.Data[1, 0] + sceneY * H.Data[1, 1] + H.Data[1, 2];
                    var Z = sceneX * H.Data[2, 0] + sceneY * H.Data[2, 1] + H.Data[2, 2];

                    var u = X / Z; var v = Y / Z;

                    var scenePoint = new MCvPoint3D64f(sceneX, sceneY, 0);
                    var imagePoint = new MCvPoint2D64f(u, v);

                    points.Add(new GridPoint(scenePoint, imagePoint));
                }
            }

            return points;
        }

        #endregion

        #region Defines the logic to distort the points

        /// <summary>
        /// Defines the logic to distort the points
        /// </summary>
        public void DistortPoints(double k1)
        {
            // Update the settings
            _settings.Distortion = k1;

            // Build the distortion
            var distortion = new Distortion(k1, _imageSize.Width + _imageSize.Height, _imageSize.Width / 2, _imageSize.Height / 2);
            var distorter = new Distorter();

            // Find the adjusted corners
            foreach (var corner in _clickedPoints.GetPoints()) corner.ImagePoint = distorter.UnDistort(distortion, corner.OriginalImagePoint);

            // Generate a new set of points
            _points = GeneratePoints();

            // Apply distortion to each of the points
            foreach (var point in _points) point.ImagePoint = distorter.Distort(distortion, point.OriginalImagePoint);

            // Tell the system that the points have been updated
            if (OnChange != null) OnChange();
        }

        #endregion

        #region Update the Square Size

        /// <summary>
        /// Update the size of the square
        /// </summary>
        /// <param name="value">The value that is being updated</param>
        public void ChangeSquare(int value)
        {
            _settings.SquareSize = value;
            if (OnChange != null) OnChange();
        }

        #endregion
    }
}