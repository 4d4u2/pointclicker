﻿using PointClicker.Model;
using PointClicker.View;

namespace PointClicker.Controller
{
    /// <summary>
    /// Defines a controller for the associated settings
    /// </summary>
    public class SettingsController
    {
        #region Private Variables

        private GridSettings _settings;

        #endregion

        #region Constructors

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="settings">The settings that we are updating</param>
        public SettingsController(GridSettings settings)
        {
            _settings = settings;
        }

        #endregion

        #region Getter for the settings

        /// <summary>
        /// Retrieve the associated list of settings
        /// </summary>
        /// <returns>The settings that we are getting</returns>
        public GridSettings GetSettings()
        {
            return _settings;
        }

        #endregion

        #region Update Functionality

        /// <summary>
        /// Update the controls with the settings values
        /// </summary>
        /// <param name="settingsForm">The form that we are updating</param>
        public void UpdateControls(SettingsForm settingsForm)
        {
            settingsForm.GridXBox.Text = _settings.GridX.ToString();
            settingsForm.GridYBox.Text = _settings.GridY.ToString();
            settingsForm.BlockBox.Text = _settings.BlockSize.ToString();
            settingsForm.SquareBox.Text = _settings.SquareSize.ToString();
        }

        #endregion

        #region Validation Check

        /// <summary>
        /// Validate the grids size value
        /// </summary>
        /// <param name="value">The value that we are checking</param>
        /// <returns>Empty if no error, else the associated error string</returns>
        public string ValidateGridSize(string value)
        {

            int integer; var success = int.TryParse(value, out integer);
            if (!success) return string.Format("{0} is not an integer!", value);
            if (integer < 0 || integer > 100) return "Must be in range 0 to 100";
            return string.Empty;
        }

        /// <summary>
        /// Validate the block size value
        /// </summary>
        /// <param name="value">The value that we are checking</param>
        /// <returns>Empty if no error, else the associated error string</returns>
        public string ValidateBlockSize(string value)
        {
            double floatValue; var success = double.TryParse(value, out floatValue);
            if (!success) return string.Format("{0} is not a number!", value);
            if (floatValue < 1 || floatValue > 1000) return "Must be in range 1 to 1000";
            return string.Empty;
        }

        /// <summary>
        /// Validate the square size value
        /// </summary>
        /// <param name="value">The value that we are checking</param>
        /// <returns>Empty if no error, else the associated error string</returns>
        public string ValidateSquareSize(string value)
        {
            int integer; var success = int.TryParse(value, out integer);
            if (!success) return string.Format("{0} is not an integer!", value);
            if (integer < 3 || integer > 1000) return "Must be in range 0 to 1000";
            return string.Empty;
        }

        #endregion

        #region Submit Functionality

        /// <summary>
        /// Submit logic associated with the form
        /// </summary>
        /// <param name="form">The form that contains the fields</param>
        /// <returns>Success(true) or Failure(false)</returns>
        public bool Submit(SettingsForm form)
        {
            form.Provider.Clear();

            var error1 = ValidateGridSize(form.GridXBox.Text);
            form.Provider.SetError(form.GridXBox, error1);
            var error2 = ValidateGridSize(form.GridYBox.Text);
            form.Provider.SetError(form.GridYBox, error2);
            var error3 = ValidateBlockSize(form.BlockBox.Text);
            form.Provider.SetError(form.BlockBox, error3);
            var error4 = ValidateBlockSize(form.SquareBox.Text);
            form.Provider.SetError(form.SquareBox, error4);
            var totalError = error1 + error2 + error3 + error4;
            if (!string.IsNullOrWhiteSpace(totalError)) return false;

            _settings.GridX = int.Parse(form.GridXBox.Text);
            _settings.GridY = int.Parse(form.GridYBox.Text);
            _settings.BlockSize = int.Parse(form.BlockBox.Text);
            _settings.SquareSize = int.Parse(form.SquareBox.Text);

            return true;
        }

        #endregion
    }
}
