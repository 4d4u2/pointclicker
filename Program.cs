﻿using PointClicker.View;
using System;
using System.Windows.Forms;

namespace PointClicker
{
    /// <summary>
    /// Main entry point module for the application
    /// </summary>
    public static class Program
    {
        #region Entry Point Method

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }

        #endregion
    }
}